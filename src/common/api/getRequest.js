import axiosConfig from "./axiosConfig";

export function requestNewReleases() {
  return axiosConfig("new-releases", "albums");
}

export function requestFeaturedPlaylists() {
  return axiosConfig("featured-playlists", "playlists");
}

export function requestCategories() {
  return axiosConfig("categories", "categories");
}
