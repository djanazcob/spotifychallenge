import axios from "axios";
import config from "../../config";

const body = "grant_type=client_credentials";

export default async function axiosConfig(path, type) {
  const {
    data: { access_token: token },
  } = await axios.post(config.api.authUrl, body, {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${btoa(
        `${config.api.clientId}:${config.api.clientSecret}`
      )}`,
    },
  });

  const response = await axios.get(`${config.api.baseUrl}/browse/${path}`, {
    headers: { Authorization: `Bearer ${token}` },
  });

  return response.data[type].items;
}
